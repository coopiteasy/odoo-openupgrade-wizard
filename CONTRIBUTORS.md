# Developers

* Sylvain LE GAL, from GRAP (http://www.grap.coop)
* Rémy TAYMANS, from Coop It Easy (https://coopiteasy.be/)
* Cyril JEANNERET, from Camptocamp (https://www.camptocamp.com)
* Simon MAILLARD

# Reviewers

* Sébastien BEAU, from Akretion (https://akretion.com)
