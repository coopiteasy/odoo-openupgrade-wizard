Add a new option ``--postgresql-version`` in ``oow init`` command to
define the version of the postgresql image to be used for the project.
